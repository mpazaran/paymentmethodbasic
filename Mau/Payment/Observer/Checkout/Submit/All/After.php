<?php

namespace Mau\Payment\Observer\Checkout\Submit\All;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Mau\Payment\Api\MauPaymentRepositoryInterface;

class After implements ObserverInterface
{
    /**
     * @var MauPaymentRepositoryInterface
     */
    private $paymentRepository;
    /**
     * @var TimezoneInterface
     */
    private $timezoneInterface;

    /**
     * After constructor.
     * @param MauPaymentRepositoryInterface $paymentRepository
     * @param TimezoneInterface             $timezoneInterface
     */
    public function __construct(
        MauPaymentRepositoryInterface $paymentRepository,
        TimezoneInterface $timezoneInterface
    )
    {
        $this->paymentRepository = $paymentRepository;
        $this->timezoneInterface = $timezoneInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var OrderInterface $order */
        $order       = $observer->getData('order');
        $paymentData = $this->paymentRepository->create();
        $paymentData->setIncrementId($order->getIncrementId());
        $paymentData->setCustomerName($order->getCustomerFirstname() . ' ' . $order->getCustomerLastname());
        $paymentData->setCreatedAt($this->timezoneInterface->date()->format('Y-m-d H:i:s'));
        $this->paymentRepository->save($paymentData);
        return $this;
    }
}
