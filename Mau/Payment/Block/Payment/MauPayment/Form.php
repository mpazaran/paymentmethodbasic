<?php

namespace Mau\Payment\Block\Payment\MauPayment;

class Form extends \Magento\Payment\Block\Form
{
    /**
     * Checkmo template
     *
     * @var string
     */
    protected $_template = 'Mau_Payment::payment/mau_payment/form.phtml';
}


