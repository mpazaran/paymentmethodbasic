<?php

namespace Mau\Payment\Logger\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger as MonoLogger;

class Logger extends Base
{

    /**
     * @var string
     */
    protected $fileName = '/var/log/mau_payment.log';

    /**
     * @var
     */
    protected $loggerType = MonoLogger::DEBUG;

}
