<?php

namespace Mau\Payment\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface MauPaymentSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \Mau\Payment\Api\Data\MauPaymentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
