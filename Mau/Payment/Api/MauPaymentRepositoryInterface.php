<?php

namespace Mau\Payment\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @package Mau\Payment\Api
 */
interface MauPaymentRepositoryInterface extends OptionSourceInterface
{

    /**
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     */
    public function create();

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \Mau\Payment\Api\MauPaymentSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @return \Mau\Payment\Model\ResourceModel\MauPayment\Collection
     */
    public function getCollection();

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null);

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\Mau\Payment\Api\Data\MauPaymentInterface $model, $value, $field = null);

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     */
    public function save(\Mau\Payment\Api\Data\MauPaymentInterface $model);

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     */
    public function delete(\Mau\Payment\Api\Data\MauPaymentInterface $model);

    /**
     * @param int $id
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null);

}
