<?php

namespace Mau\Payment\Api\Data;

/**
 * @package Mau\Payment\Api
 */
interface MauPaymentInterface
{

    /**
     * @param array $fields []
     *
     * @return array
     */
    public function toArray(array $fields = []);


    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return MauPaymentInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getIncrementId();

    /**
     * @param string $incrementId
     *
     * @return MauPaymentInterface
     */
    public function setIncrementId($incrementId);

    /**
     * @return string
     */
    public function getCustomerName();

    /**
     * @param string $customerName
     *
     * @return MauPaymentInterface
     */
    public function setCustomerName($customerName);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     *
     * @return MauPaymentInterface
     */
    public function setCreatedAt($createdAt);
}


