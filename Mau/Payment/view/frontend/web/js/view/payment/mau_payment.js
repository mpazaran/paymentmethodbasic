define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'mau_payment',
                component: 'Mau_Payment/js/view/payment/method-renderer/mau_payment'
            }
        );
        return Component.extend({});
    }
);

