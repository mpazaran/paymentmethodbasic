define(
    [
        "jquery",
        'Magento_Checkout/js/view/payment/default',
        'Magento_Ui/js/modal/modal'
    ],
    function ($, Component, modal) {
        'use strict';

        return Component.extend({
            defaults         : {
                template: 'Mau_Payment/payment/mau_payment'
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            askPlaceOrder    : function () {
                let self = this;
                $('#mau-payment-modal').modal(
                    {
                        type            : 'popup',
                        responsive      : true,
                        innerScroll     : true,
                        clickableOverlay: false,
                        modalClass      : 'priceoptions',
                        title           : $.mage.__('Place Order'),
                        buttons         : [
                            {
                                text : $.mage.__('Close'),
                                class: 'close',
                                id   : 'close',
                                click: function () {
                                    this.closeModal();
                                }
                            },
                            {
                                text : $.mage.__('Place Order Here'),
                                class: 'close',
                                id   : 'close',
                                click: function () {
                                    this.closeModal();
                                    self.placeOrder();
                                }
                            }
                        ]
                    }
                ).modal('openModal')
            }
        });
    }
);

