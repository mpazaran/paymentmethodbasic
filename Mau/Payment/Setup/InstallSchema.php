<?php

namespace Mau\Payment\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * The schema initialization stages are the first set of processes Magento runs when your module is installed, re-installed, or upgraded.
 *
 * Magento executes the schema installation class during your module’s initial install.
 * If the schema_version for your module is found in the setup_module table,
 * Magento skips this stage and proceeds to the schema upgrade stage.
 *
 * @author {{author}}
 * @see https://devdocs.magento.com/guides/v2.0/extension-dev-guide/prepare/lifecycle.html
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @inheritdoc
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if(!$setup->tableExists('mau_payment')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('mau_payment'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Row ID'
                )
                ->addColumn(
                    'increment_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Order Number'
                )
                ->addColumn(
                    'customer_name',
                    Table::TYPE_INTEGER,
                    4,
                    ['nullable' => false],
                    'Customer Name'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Createad At'
                );
            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();

    }
}
