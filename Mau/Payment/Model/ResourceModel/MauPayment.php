<?php

namespace Mau\Payment\Model\ResourceModel;

/**
 * Relates the mysql table mau_payment with the model \Mau\Payment\Model\MauPayment.
 *
 */
class MauPayment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritdoc
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('mau_payment', 'id');
    }
}

