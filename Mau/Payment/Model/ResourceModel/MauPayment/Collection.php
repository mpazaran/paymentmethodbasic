<?php

namespace Mau\Payment\Model\ResourceModel\MauPayment;

/**
 * Collection for \Mau\Payment\Model\MauPayment.
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'mau_payment_mau_payment_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'mau_payment_mau_payment_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Mau\Payment\Model\MauPayment::class, \Mau\Payment\Model\ResourceModel\MauPayment::class);
    }

}


