<?php

/** @noinspection PhpDeprecationInspection */

namespace Mau\Payment\Model\Payment;

use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Mau\Payment\Block\Payment\MauPayment\Form;
use Mau\Payment\Block\Payment\MauPayment\Info;


class MauPayment extends AbstractMethod
{

    const CODE = 'mau_payment';

    /**
     * Payment method code
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var string
     */
    protected $_formBlockType = Form::class;

    /**
     * @var string
     */
    protected $_infoBlockType = Info::class;

    /**
     * Availability option
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * @var State
     */
    protected $state;

    /**
     * @param Context                    $context
     * @param Registry                   $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory      $customAttributeFactory
     * @param Data                       $paymentData
     * @param ScopeConfigInterface       $scopeConfig
     * @param Logger                     $logger
     * @param AbstractResource|null      $resource
     * @param AbstractDb|null            $resourceCollection
     * @param array                      $data
     * @param State                      $state
     * @param DirectoryHelper|null       $directory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        State $state,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    )
    {
        parent::__construct(
            $context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $resource, $resourceCollection, $data, $directory
        );
        $this->state = $state;
    }

    public function isActive($storeId = null)
    {
        if ($this->state->getAreaCode() === Area::AREA_FRONTEND) {
            return (bool)$this->getConfigData('active', $storeId) && !((bool)$this->getConfigData('only_backend', $storeId));
        }
        return (bool)$this->getConfigData('active', $storeId);
    }
}
