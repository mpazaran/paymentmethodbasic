<?php

namespace Mau\Payment\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Mau\Payment\Api\Data\MauPaymentInterface;

/**
 * This is the table mau_payment representation in Magento.
 *
 * @package Mau\Payment\Model
 */
class MauPayment extends AbstractModel implements IdentityInterface, MauPaymentInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     */
    const CACHE_TAG = 'mau_payment_mau_payment';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    protected $_cacheTag = 'mau_payment_mau_payment';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_eventPrefix = 'mau_payment_mau_payment';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\MauPayment::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param int $id
     *
     * @return MauPaymentInterface
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * @return string
     */
    public function getIncrementId()
    {
        return $this->getData('increment_id');
    }

    /**
     * @param string $incrementId
     *
     * @return MauPaymentInterface
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData('increment_id', $incrementId);
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getData('customer_name');
    }

    /**
     * @param string $customerName
     *
     * @return MauPaymentInterface
     */
    public function setCustomerName($customerName)
    {
        return $this->setData('customer_name', $customerName);
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData('created_at');
    }

    /**
     * @param string $createdAt
     *
     * @return MauPaymentInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData('created_at', $createdAt);
    }

}
