<?php

namespace Mau\Payment\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Mau\Payment\Api\MauPaymentRepositoryInterface as ApiMauPaymentRepositoryInterface;
use Mau\Payment\Api\MauPaymentSearchResultsInterfaceFactory as ApiMauPaymentSearchResultsInterface;
use Mau\Payment\Model\ResourceModel\MauPaymentFactory as ResourceModelMauPaymentFactory;
use Mau\Payment\Model\ResourceModel\MauPayment as ResourceModelMauPayment;


/**
 * This is the repository for MauPayment
 *
 * @package Mau\Payment\Model\repository
 */
class MauPaymentRepository implements ApiMauPaymentRepositoryInterface
{

    /**
     * @var MauPaymentFactory
     */
    protected $modelFactory         = null;

    /**
     * @var ResourceModelMauPaymentFactory
     */
    protected $resourceModelFactory = null;

    /**
     * @var ResourceModelMauPayment
     */
    protected $resourceModel = null;

    /**
     * @var $searchResultsFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceModelMauPayment\CollectionFactory
     */
    protected $collectionFactory    = null;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param MauPaymentFactory $modelFactory
     * @param ResourceModelMauPaymentFactory $resourceModelFactory
     * @param ResourceModelMauPayment\CollectionFactory $collectionFactory
     * @param ApiMauPaymentSearchResultsInterface $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        MauPaymentFactory $modelFactory,
        ResourceModelMauPaymentFactory $resourceModelFactory,
        ResourceModelMauPayment\CollectionFactory $collectionFactory,
        ApiMauPaymentSearchResultsInterface $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    )
    {
        $this->modelFactory          = $modelFactory;
        $this->resourceModelFactory  = $resourceModelFactory;
        $this->resourceModel         = $resourceModelFactory->create();
        $this->searchResultsFactory  = $searchResultsFactory;
        $this->collectionFactory     = $collectionFactory;
        $this->collectionProcessor   = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
    }

    /**
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     */
    public function create()
    {
        return $this->modelFactory->create();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \Mau\Payment\Api\MauPaymentSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null)
    {
        $searchResults = $this->searchResultsFactory->create();
        $collection = $this->getCollection();
        if (null !== $searchCriteria) {
            $searchResults->setSearchCriteria($searchCriteria);
            $this->collectionProcessor->process($searchCriteria, $collection);
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($collection->getItems());
    }

    /**
     * @return \Mau\Payment\Model\ResourceModel\MauPayment\Collection
     */
    public function getCollection(){
        return $this->collectionFactory->create();
    }

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null) {
        return $this->loadModel($this->modelFactory->create(), $value, $field);
    }

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\Mau\Payment\Api\Data\MauPaymentInterface $model, $value, $field = null) {
        $this->resourceModel->load($model, $value, $field);
        if(!$model->getId()) {
            throw new NoSuchEntityException(__('The register not longer exists.'));
        }
        return $model;
    }

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws \Exception
     */
    public function save(\Mau\Payment\Api\Data\MauPaymentInterface $model) {
        $this->resourceModel->save($model);
        return $model;
    }

    /**
     * @param \Mau\Payment\Api\Data\MauPaymentInterface $model
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws \Exception
     */
    public function delete(\Mau\Payment\Api\Data\MauPaymentInterface $model) {
        $this->resourceModel->delete($model);
        return $model;
    }

    /**
     * @param int $id
     *
     * @return \Mau\Payment\Api\Data\MauPaymentInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id) {
        $model = $this->load($id);
        $this->resourceModel->delete($model);
        return $model;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null)
    {
        $options = [];
        foreach($this->getList($searchCriteria)->getItems() as $model) {
            $options[] = [
                'value' => $model->getId(),
                'label' => $model->getId(),
            ];
        }

        return $options;
    }

}
